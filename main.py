from splinter import Browser
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

OBS = "França"
ELEMS = {"entrada1": "09:00", "saida1": "12:00",
         "entrada2": "13:00", "saida2": "18:00"}
USER = ""
PASS = ""


def preencher_horas(browser):
    browser.find_element_by_id("observacoes").send_keys(OBS)
    script = "document.getElementById('{}').value = '{}';"
    for k, v in ELEMS.items():
        browser.execute_script(script).format(k, v)
        #elem = browser.find_element_by_id(k)
        # elem.click()
        # elem.send_keys(value)

    browser.find_element_by_id("salvar").click()
    time.sleep(4)

    if EC.alert_is_present():
        alert = browser.switch_to_alert()
        alert.accept()
        browser.find_element_by_id("voltar").click()
    time.sleep(6)


def acessar_dias(browser):
    qtd_dias = len(browser.find_elements_by_class_name("incompleto"))
    dias = range(qtd_dias)
    for dia in dias:
        elem = browser.find_elements_by_class_name("incompleto")[dia]
        elem.click()
        time.sleep(4)
        preencher_horas(browser)


def login(browser):
    browser.get("https://www.secullum.com.br/Ponto4Web/822470761#login")
    time.sleep(4)
    browser.find_element_by_name("usuario").send_keys(USER)
    browser.find_element_by_name("senha").send_keys(PASS)
    browser.find_element_by_id("login").click()
    time.sleep(4)


def main():
    browser = webdriver.Chrome()
    login(browser)
    acessar_dias(browser)


if __name__ == '__main__':
    main()
